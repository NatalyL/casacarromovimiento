package org.nlopez;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

/**
 * CasaCarro.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel)
 * <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class CasaCarro extends JFrame implements GLEventListener, KeyListener, MouseMotionListener, MouseListener {

    Casa c;
    Arbol a;
    Carro cr;
    Nube n;

    private static float trasladaY = 0;
    private static float trasladaX = 0;
    private static float rotar = 0;
    private static boolean dirLados = false;
    private static float tx = 0, ty = 0, cenx = 0, ceny = 0;

    public static void main(String[] args) {

        Frame frame = new Frame("Casa");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new CasaCarro());
        frame.add(canvas);
        frame.setSize(640, 480);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();

    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.

        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        addKeyListener(this);

        c = new Casa();
        a = new Arbol();
        cr = new Carro();
        n = new Nube();

        drawable.addKeyListener(this);
        addMouseMotionListener(this);
        drawable.addMouseMotionListener(this);
        addMouseListener(this);
        drawable.addMouseListener(this);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!

            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        // Move the "drawing cursor" around
        gl.glTranslatef(-1.5f, 0.0f, -6.0f);

        //Cielo
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.2f, 0.9f, 0.8f);

        gl.glVertex3f(-2.1f, 2.5f, 0.0f);
        gl.glVertex3f(5.1f, 2.5f, 0.0f);

        gl.glColor3f(0.3f, 0.6f, 0.8f);

        gl.glVertex3f(5.1f, -1.0f, 0.0f);
        gl.glVertex3f(-2.1f, -1.0f, 0.0f);
        gl.glEnd();

        // NUBE
        gl.glPushMatrix();
        gl.glTranslatef(tx / 90, -ty / 90, 0);
        gl.glTranslatef(0, 0.1f, 0);
        n.Dibujar(gl);
        gl.glEnd();
        gl.glPopMatrix();

        //Suelo
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.3f, 0.7f, 0.1f);

        gl.glVertex3f(-2.1f, -1.0f, 0.0f);
        gl.glVertex3f(5.1f, -1.0f, 0.0f);

        gl.glColor3f(0.3f, 1.0f, 0.3f);

        gl.glVertex3f(5.1f, -2.48f, 0.0f);
        gl.glVertex3f(-2.1f, -2.48f, 0.0f);
        gl.glEnd();

        //Calles
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.8f, 0.8f, 0.8f);

        gl.glVertex3f(-2.1f, -1.46f, 0.0f);
        gl.glVertex3f(-2.1f, -2.0f, 0.0f);
        gl.glVertex3f(5.1f, -2.0f, 0.0f);
        gl.glVertex3f(5.1f, -1.46f, 0.0f);

        //Entrada garage
        gl.glColor3f(0.5f, 0.6f, 0.4f);

        gl.glVertex3f(-0.9f, -1.1f, 0.0f);
        gl.glVertex3f(-1.1f, -1.46f, 0.0f);
        gl.glVertex3f(0.9f, -1.46f, 0.0f);
        gl.glVertex3f(0.7f, -1.1f, 0.0f);
        gl.glEnd();

        //CASA
        c.Dibujar(gl);

        //ARBOL
        a.Dibujar(gl);

        //CARRO
        gl.glPushMatrix();

        if (dirLados == true) {
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glRotatef(rotar, 0, 1, 0);
            cr.Dibujar1(gl);
        } else {
            gl.glTranslatef(trasladaX, trasladaY, 0);
            cr.Dibujar(gl);
        }
        gl.glEnd();
        gl.glPopMatrix();

        cenx = tx + 515f;
        ceny = ty + 70f;
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    public void keyTyped(KeyEvent ke) {

    }

    public void keyPressed(KeyEvent ke) {

        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            if (trasladaY <= -0.1f) {
                if (trasladaX <= 0.4f && trasladaX >= -0.4f) {
                    trasladaY += .1f;
                    System.out.println("Valor en la traslacion de Y: " + trasladaY);
                }
            }
            dirLados = false;
        }

        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            if (trasladaY >= -0.8f) {
                if (trasladaX <= 0.4f && trasladaX >= -0.4f) {
                    trasladaY -= .1f;
                    System.out.println("Valor en la traslacion de X: " + trasladaY);
                }
            }
            dirLados = false;
        }

        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (trasladaX <= 4.1f) {
                if (trasladaY <= -0.7f && trasladaY >= -1.0f) {
                    trasladaX += .1f;
                    System.out.println("Valor en la traslacion de X: " + trasladaX);
                }
            }
            dirLados = true;
            rotar = 180f;
        }

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            if (trasladaX >= -1.1f) {
                if (trasladaY <= -0.7f && trasladaY >= -1.0f) {
                    trasladaX -= .1f;
                    System.out.println("Valor en la traslacion de X: " + trasladaX);
                }
            }
            dirLados = true;
            rotar = 360f;
        }
    }

    public void keyReleased(KeyEvent ke) {

    }

    public void mouseDragged(MouseEvent me) {
    }

    public void mouseMoved(MouseEvent me) {

        while (me.getX() > cenx) {
            tx += 1f;
            cenx += 1f;
        }
        while (me.getX() < cenx) {
            tx -= 1f;
            cenx -= 1f;
        }
        while (me.getY() > ceny) {
            ty += 1f;
            ceny = ceny + 1f;
            System.out.println(me.getY());
            System.out.println("cen" + ceny);
        }
        while (me.getY() < ceny) {
            ty -= 1f;
            ceny = ceny - 1f;
            System.out.println(me.getY());
            System.out.println("cen" + ceny);
        }
    }

    public void mouseClicked(MouseEvent me) {
//        if (me.getButton() == MouseEvent.BUTTON1) {
//            if (me.getX() > cenx) {
//                tx += 10f;
//                cenx += 10f;
//            }
//            if (me.getX() < cenx) {
//                tx -= 10f;
//                cenx -= 10f;
//            }
//            if (me.getY() > ceny) {
//                ty += 10f;
//                ceny = ceny + 10f;
//                System.out.println(me.getY());
//                System.out.println("cen" + ceny);
//            }
//            if (me.getY() < ceny) {
//                ty -= 10f;
//                ceny = ceny - 10f;
//                System.out.println(me.getY());
//                System.out.println("cen" + ceny);
//            }
//        }
    }

    public void mousePressed(MouseEvent me) {
    }

    public void mouseReleased(MouseEvent me) {
    }

    public void mouseEntered(MouseEvent me) {
    }

    public void mouseExited(MouseEvent me) {

    }
}
