/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Figura {
    
    public static final double Pi = 3.14159265358979323846;
    
    public void Circulo(float posx, float posy, float radio, GL gl) {
        float x, y;
        gl.glBegin(GL.GL_POLYGON);
//        gl.glColor3f(1.0f, 1.0f, 1.0f);
        for (float i = 0; i < 10; i += 0.01) {
            x = (float) (radio * Math.cos(i));
            y = (float) (radio * Math.sin(i));
            gl.glVertex3f(x / 2 + posx, y / 2 + posy, 0.0f);
        }
        gl.glEnd();
    }
    
    public void SemiCirculo(float posx, float posy, float radio, GL gl) {
        float x, y;
        gl.glBegin(GL.GL_POLYGON);
//        gl.glColor3f(1.0f, 1.0f, 1.0f);
        for (float i = 0; i < 3.1f; i += 0.01) {
            x = (float) (radio * Math.cos(i));
            y = (float) (radio * Math.sin(i));
            gl.glVertex3f(x / 2 + posx, y / 2 + posy, 0.0f);
        }
        gl.glEnd();
    }
}
