/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Casa {

    public void Dibujar(GL gl) {

        //Dibuja techo del garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.1f, 0.1f, 0.5f);

        gl.glVertex3f(-1.3f, 0.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.3f, 0.0f);
        gl.glVertex3f(0.9f, 0.3f, 0.0f);
        gl.glVertex3f(0.9f, 0.0f, 0.0f);
        gl.glEnd();

        //Dibuja el cuadrado garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.7f, 0.5f, 0.4f);

        gl.glVertex3f(-1.1f, -1.1f, 0.0f);
        gl.glVertex3f(-1.1f, 0.0f, 0.0f);
        gl.glVertex3f(0.9f, 0.0f, 0.0f);
        gl.glVertex3f(0.9f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja el garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(.3f, .3f, .3f);

        gl.glVertex3f(-0.9f, -1.1f, 0.0f);
        gl.glVertex3f(-0.9f, -0.2f, 0.0f);
        gl.glVertex3f(0.7f, -0.2f, 0.0f);
        gl.glVertex3f(0.7f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja el cuadrado sobre garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.7f, 0.5f, 0.4f);

        gl.glVertex3f(0.0f, 1.0f, 0.0f);
        gl.glVertex3f(0.0f, 0.3f, 0.0f);
        gl.glVertex3f(0.9f, 0.3f, 0.0f);
        gl.glVertex3f(0.9f, 1.0f, 0.0f);
        gl.glEnd();

        //Dibuja la ventana sobre garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(0.1f, 0.9f, 0.0f);
        gl.glVertex3f(0.1f, 0.4f, 0.0f);
        gl.glVertex3f(0.8f, 0.4f, 0.0f);
        gl.glVertex3f(0.8f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja el cristal de ventana sobre garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(0.15f, 0.85f, 0.0f);
        gl.glVertex3f(0.15f, 0.45f, 0.0f);
        gl.glVertex3f(0.75f, 0.45f, 0.0f);
        gl.glVertex3f(0.75f, 0.85f, 0.0f);
        gl.glEnd();

        //Dibuja marco de la ventana horizontal sobre garaje
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(0.1f, 0.75f, 0.0f);
        gl.glVertex3f(0.1f, 0.7f, 0.0f);
        gl.glVertex3f(0.8f, 0.7f, 0.0f);
        gl.glVertex3f(0.8f, 0.75f, 0.0f);
        gl.glEnd();

        //Dibuja marco de la ventana vertical 1 sobre garaje 
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(0.3f, 0.9f, 0.0f);
        gl.glVertex3f(0.3f, 0.4f, 0.0f);
        gl.glVertex3f(0.35f, 0.4f, 0.0f);
        gl.glVertex3f(0.35f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja marco de la ventana vertical 2 sobre garaje 
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(0.53f, 0.9f, 0.0f);
        gl.glVertex3f(0.53f, 0.4f, 0.0f);
        gl.glVertex3f(0.58f, 0.4f, 0.0f);
        gl.glVertex3f(0.58f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja triangulo posterior
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor3f(0.1f, 0.1f, 0.5f);

        gl.glVertex2f(-0.2f, 1.0f);
        gl.glVertex2f(2.8f, 1.0f);
        gl.glVertex2f(1.5f, 1.6f);
        gl.glEnd();

        //Dibuja el cuadrado cuartos
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.75f, 0.6f, 0.4f);

        gl.glVertex3f(0.9f, -1.1f, 0.0f);
        gl.glVertex3f(0.9f, 1.0f, 0.0f);
        gl.glVertex3f(2.8f, 1.0f, 0.0f);
        gl.glVertex3f(2.8f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja el cuadrado puerta
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.6f, 0.4f, 0.35f);

        gl.glVertex3f(0.9f, -1.1f, 0.0f);
        gl.glVertex3f(0.9f, 0.1f, 0.0f);
        gl.glVertex3f(2.05f, 0.1f, 0.0f);
        gl.glVertex3f(2.05f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja la puerta
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.4f, 0.2f, 0.1f);

        gl.glVertex3f(1.25f, -1.1f, 0.0f);
        gl.glVertex3f(1.25f, -0.3f, 0.0f);
        gl.glVertex3f(1.7f, -0.3f, 0.0f);
        gl.glVertex3f(1.7f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja la manija
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.85f, 0.85f, 0.0f);

        gl.glVertex3f(1.3f, -0.75f, 0.0f);
        gl.glVertex3f(1.3f, -0.7f, 0.0f);
        gl.glVertex3f(1.35f, -0.7f, 0.0f);
        gl.glVertex3f(1.35f, -0.75f, 0.0f);
        gl.glEnd();

        //Dibuja el cuadrado ventana 1
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(1.1f, 0.9f, 0.0f);
        gl.glVertex3f(1.1f, 0.4f, 0.0f);
        gl.glVertex3f(1.9f, 0.4f, 0.0f);
        gl.glVertex3f(1.9f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja el cristal de ventana 1
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(1.15f, 0.85f, 0.0f);
        gl.glVertex3f(1.15f, 0.45f, 0.0f);
        gl.glVertex3f(1.85f, 0.45f, 0.0f);
        gl.glVertex3f(1.85f, 0.85f, 0.0f);
        gl.glEnd();

        //Dibuja el marco horizontal ventana 1
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(1.1f, 0.75f, 0.0f);
        gl.glVertex3f(1.1f, 0.7f, 0.0f);
        gl.glVertex3f(1.9f, 0.7f, 0.0f);
        gl.glVertex3f(1.9f, 0.75f, 0.0f);
        gl.glEnd();

        //Dibuja el marco vertical 1 ventana 1
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(1.35f, 0.9f, 0.0f);
        gl.glVertex3f(1.35f, 0.4f, 0.0f);
        gl.glVertex3f(1.4f, 0.4f, 0.0f);
        gl.glVertex3f(1.4f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja el marco vertical 2 ventana 1
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(1.63f, 0.9f, 0.0f);
        gl.glVertex3f(1.63f, 0.4f, 0.0f);
        gl.glVertex3f(1.68f, 0.4f, 0.0f);
        gl.glVertex3f(1.68f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja el ventana 2
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(2.2f, 0.9f, 0.0f);
        gl.glVertex3f(2.2f, 0.25f, 0.0f);
        gl.glVertex3f(2.65f, 0.25f, 0.0f);
        gl.glVertex3f(2.65f, 0.9f, 0.0f);
        gl.glEnd();

        //Dibuja el cristal de ventana 2
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(2.25f, 0.85f, 0.0f);
        gl.glVertex3f(2.25f, 0.3f, 0.0f);
        gl.glVertex3f(2.6f, 0.3f, 0.0f);
        gl.glVertex3f(2.6f, 0.85f, 0.0f);
        gl.glEnd();

        //Dibuja el maro ventana 2
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(2.2f, 0.75f, 0.0f);
        gl.glVertex3f(2.2f, 0.7f, 0.0f);
        gl.glVertex3f(2.65f, 0.7f, 0.0f);
        gl.glVertex3f(2.65f, 0.75f, 0.0f);
        gl.glEnd();

        //Dibuja el ventana 3
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(2.2f, -0.85f, 0.0f);
        gl.glVertex3f(2.2f, -0.2f, 0.0f);
        gl.glVertex3f(2.65f, -0.2f, 0.0f);
        gl.glVertex3f(2.65f, -0.85f, 0.0f);
        gl.glEnd();

        //Dibuja el cristal de ventana 3
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(2.25f, -0.25f, 0.0f);
        gl.glVertex3f(2.25f, -0.8f, 0.0f);
        gl.glVertex3f(2.6f, -0.8f, 0.0f);
        gl.glVertex3f(2.6f, -0.25f, 0.0f);
        gl.glEnd();

        //Dibuja el marco ventana 3
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(2.2f, -0.4f, 0.0f);
        gl.glVertex3f(2.2f, -0.35f, 0.0f);
        gl.glVertex3f(2.65f, -0.35f, 0.0f);
        gl.glVertex3f(2.65f, -0.4f, 0.0f);
        gl.glEnd();

        //Dibuja triangulo 
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor3f(0.15f, 0.3f, 0.7f);

        gl.glVertex2f(0.7f, 1.0f);
        gl.glVertex2f(3.0f, 1.0f);
        gl.glVertex2f(1.85f, 1.6f);
        gl.glEnd();

    }
}
