/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Nube {

    private static float tx = 0, ty = 0, cenx = 0, ceny = 0;
    public void Dibujar(GL gl) {

        Figura f = new Figura();

        gl.glPushMatrix();
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        f.Circulo(3.25f, 1.5f, 0.6f, gl);
        f.Circulo(3.65f, 1.56f, 0.75f, gl);
        f.Circulo(3.99f, 1.66f, 0.5f, gl);
        f.Circulo(4.1f, 1.48f, 0.6f, gl);
        f.Circulo(4.29f, 1.53f, 0.48f, gl);
        gl.glEnd();
        gl.glPopMatrix();
    }
}
