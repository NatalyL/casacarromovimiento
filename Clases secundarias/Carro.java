/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Carro {

    public void Dibujar(GL gl) {

        //Dibuja parte superior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.1f, 0.1f);

        gl.glVertex3f(-0.35f, -0.55f, 0.0f);
        gl.glVertex3f(-0.5f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -0.75f, 0.0f);
        gl.glVertex3f(0.15f, -0.55f, 0.0f);
        gl.glEnd();

        //Dibuja parabrisas del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(-0.35f, -0.58f, 0.0f);
        gl.glVertex3f(-0.47f, -0.75f, 0.0f);
        gl.glVertex3f(0.26f, -0.75f, 0.0f);
        gl.glVertex3f(0.14f, -0.58f, 0.0f);
        gl.glEnd();

        //Dibuja parte inferior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.1f, 0.1f);

        gl.glVertex3f(-0.5f, -1.0f, 0.0f);
        gl.glVertex3f(-0.5f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -1.0f, 0.0f);
        gl.glEnd();

        //Dibuja frente inferior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.2f, 0.2f, 0.2f);

        gl.glVertex3f(-0.42f, -0.97f, 0.0f);
        gl.glVertex3f(-0.45f, -0.92f, 0.0f);
        gl.glVertex3f(0.26f, -0.92f, 0.0f);
        gl.glVertex3f(0.23f, -0.97f, 0.0f);
        gl.glEnd();

        //Dibuja placa del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glVertex3f(-0.2f, -0.95f, 0.0f);
        gl.glVertex3f(-0.2f, -0.9f, 0.0f);
        gl.glVertex3f(0.0f, -0.9f, 0.0f);
        gl.glVertex3f(0.0f, -0.95f, 0.0f);
        gl.glEnd();

        //Dibuja frente superior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.2f, 0.2f, 0.2f);

        gl.glVertex3f(-0.3f, -0.89f, 0.0f);
        gl.glVertex3f(-0.3f, -0.8f, 0.0f);
        gl.glVertex3f(0.1f, -0.8f, 0.0f);
        gl.glVertex3f(0.1f, -0.89f, 0.0f);
        gl.glEnd();

        //Dibuja farolas 
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.85f, 0.85f, 0.0f);

        gl.glVertex3f(-0.45f, -0.87f, 0.0f);
        gl.glVertex3f(-0.48f, -0.8f, 0.0f);
        gl.glVertex3f(-0.32f, -0.8f, 0.0f);
        gl.glVertex3f(-0.32f, -0.89f, 0.0f);

        gl.glVertex3f(0.11f, -0.89f, 0.0f);
        gl.glVertex3f(0.11f, -0.8f, 0.0f);
        gl.glVertex3f(0.28f, -0.8f, 0.0f);
        gl.glVertex3f(0.25f, -0.87f, 0.0f);
        gl.glEnd();

        //Dibuja llantas
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.0f, 0.0f, 0.0f);

        gl.glVertex3f(-0.5f, -1.1f, 0.0f);
        gl.glVertex3f(-0.5f, -1.0f, 0.0f);
        gl.glVertex3f(-0.35f, -1.0f, 0.0f);
        gl.glVertex3f(-0.35f, -1.1f, 0.0f);

        gl.glVertex3f(0.16f, -1.1f, 0.0f);
        gl.glVertex3f(0.16f, -1.0f, 0.0f);
        gl.glVertex3f(0.3f, -1.0f, 0.0f);
        gl.glVertex3f(0.3f, -1.1f, 0.0f);
        gl.glEnd();

        //Dibuja retrovisores 
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.1f, 0.1f);

        gl.glVertex3f(-0.52f, -0.7f, 0.0f);
        gl.glVertex3f(-0.49f, -0.75f, 0.0f);
        gl.glVertex3f(-0.58f, -0.75f, 0.0f);
        gl.glVertex3f(-0.58f, -0.7f, 0.0f);

        gl.glVertex3f(0.4f, -0.7f, 0.0f);
        gl.glVertex3f(0.4f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -0.75f, 0.0f);
        gl.glVertex3f(0.33f, -0.7f, 0.0f);
        gl.glEnd();
    }

    public void Dibujar1(GL gl) {

        //Dibuja parte superior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.1f, 0.1f);

        gl.glVertex3f(-0.35f, -0.55f, 0.0f);
        gl.glVertex3f(-0.55f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -0.75f, 0.0f);
        gl.glVertex3f(0.2f, -0.55f, 0.0f);
        gl.glEnd();

        //Dibuja ventanas del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.5f, 0.8f, 0.9f);

        gl.glVertex3f(-0.33f, -0.58f, 0.0f);
        gl.glVertex3f(-0.51f, -0.75f, 0.0f);
        gl.glVertex3f(-0.19f, -0.75f, 0.0f);
        gl.glVertex3f(-0.19f, -0.58f, 0.0f);

        gl.glVertex3f(0.075f, -0.58f, 0.0f);
        gl.glVertex3f(0.11f, -0.75f, 0.0f);
        gl.glVertex3f(-0.15f, -0.75f, 0.0f);
        gl.glVertex3f(-0.15f, -0.58f, 0.0f);

        gl.glVertex3f(0.115f, -0.58f, 0.0f);
        gl.glVertex3f(0.155f, -0.75f, 0.0f);
        gl.glVertex3f(0.26f, -0.75f, 0.0f);
        gl.glVertex3f(0.18f, -0.58f, 0.0f);

        gl.glEnd();

        //Dibuja parte inferior del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.1f, 0.1f);

        gl.glVertex3f(-0.55f, -1.0f, 0.0f);
        gl.glVertex3f(-0.55f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -0.75f, 0.0f);
        gl.glVertex3f(0.3f, -1.0f, 0.0f);

        gl.glVertex3f(-0.55f, -1.0f, 0.0f);
        gl.glVertex3f(-0.55f, -0.75f, 0.0f);
        gl.glVertex3f(-0.8f, -0.8f, 0.0f);
        gl.glVertex3f(-0.8f, -1.0f, 0.0f);

        gl.glEnd();

        //Dibuja guardachoques del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.25f, 0.25f, 0.25f);

        gl.glVertex3f(-0.82f, -1.0f, 0.0f);
        gl.glVertex3f(-0.82f, -0.92f, 0.0f);
        gl.glVertex3f(0.32f, -0.92f, 0.0f);
        gl.glVertex3f(0.32f, -1.0f, 0.0f);
        gl.glEnd();

        //Dibuja farolas 
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.85f, 0.85f, 0.0f);

        gl.glVertex3f(-0.79f, -0.87f, 0.0f);
        gl.glVertex3f(-0.79f, -0.82f, 0.0f);
        gl.glVertex3f(-0.7f, -0.83f, 0.0f);
        gl.glVertex3f(-0.7f, -0.86f, 0.0f);

        gl.glVertex3f(0.25f, -0.86f, 0.0f);
        gl.glVertex3f(0.25f, -0.8f, 0.0f);
        gl.glVertex3f(0.29f, -0.8f, 0.0f);
        gl.glVertex3f(0.29f, -0.87f, 0.0f);
        gl.glEnd();

        //Dibuja llantas        
        Figura f = new Figura();

        gl.glPushMatrix();
        gl.glColor3f(0.45f, 0.45f, 0.45f);
        f.SemiCirculo(-0.56f, -1.0f, 0.29f, gl);
        gl.glColor3f(0f, 0f, 0f);
        f.Circulo(-0.56f, -1.0f, 0.23f, gl);
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        f.Circulo(-0.56f, -1.0f, 0.1f, gl); //parte interna

        gl.glColor3f(0.45f, 0.45f, 0.45f);
        f.SemiCirculo(0.08f, -1.0f, 0.29f, gl);
        gl.glColor3f(0f, 0f, 0f);
        f.Circulo(0.08f, -1.0f, 0.23f, gl);
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        f.Circulo(0.08f, -1.0f, 0.1f, gl);//parte interna
        gl.glEnd();
        gl.glPopMatrix();

        //Dibuja retrovisore
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(1.0f, 0.3f, 0.4f);

        gl.glVertex3f(-0.42f, -0.7f, 0.0f);
        gl.glVertex3f(-0.42f, -0.75f, 0.0f);
        gl.glVertex3f(-0.51f, -0.75f, 0.0f);
        gl.glVertex3f(-0.48f, -0.7f, 0.0f);
        gl.glEnd();

        //Dibuja seguro del carro
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.7f, 0.7f, 0.7f);

        gl.glVertex3f(-0.27f, -0.785f, 0.0f);
        gl.glVertex3f(-0.27f, -0.8f, 0.0f);
        gl.glVertex3f(-0.205f, -0.8f, 0.0f);
        gl.glVertex3f(-0.205f, -0.785f, 0.0f);

        gl.glVertex3f(0.03f, -0.785f, 0.0f);
        gl.glVertex3f(0.03f, -0.8f, 0.0f);
        gl.glVertex3f(0.105f, -0.8f, 0.0f);
        gl.glVertex3f(0.105f, -0.785f, 0.0f);
        gl.glEnd();

        //Dibuja puertas del carro

        gl.glBegin(GL.GL_LINES);

        gl.glColor3f(0.2f, 0.2f, 0.2f);
        
        gl.glVertex2f(-0.17f, -0.58f);
        gl.glVertex2f(-0.17f, -0.785f);
        gl.glVertex2f(-0.17f, -0.785f);
        gl.glVertex2f(-0.2f, -1.0f);
        
        gl.glVertex2f(0.1f, -0.58f);
        gl.glVertex2f(0.14f, -0.785f);
        gl.glVertex2f(0.14f, -0.785f);
        gl.glVertex2f(0.1f, -0.9f);
        gl.glEnd();
    }
}
