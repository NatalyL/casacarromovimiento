/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Arbol {

    public void Dibujar(GL gl) {
        //Dibuja tronco
        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.4f, 0.2f, 0.0f);

        gl.glVertex3f(3.75f, -0.5f, 0.0f);
        gl.glVertex3f(4.05f, -0.5f, 0.0f);
        gl.glVertex3f(4.05f, -1.0f, 0.0f);
        gl.glVertex3f(3.75f, -1.0f, 0.0f);
        gl.glEnd();

        //Dibuja hojas
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor3f(0.2f, 0.8f, 0.2f);

        gl.glVertex2f(3.55f, -0.5f);
        gl.glVertex2f(3.9f, -0.5f);
        gl.glVertex2f(3.9f, 0.0f);

        gl.glColor3f(0.55f, 0.8f, 0.35f);

        gl.glVertex2f(4.25f, -0.5f);
        gl.glVertex2f(3.9f, -0.5f);
        gl.glVertex2f(3.9f, 0.0f);

        gl.glColor3f(0.35f, 0.75f, 0.35f);

        gl.glVertex2f(4.5f, 0.25f);
        gl.glVertex2f(3.3f, 0.25f);
        gl.glVertex2f(3.9f, 0.75f);

        gl.glEnd();

        gl.glBegin(GL.GL_QUADS);
        gl.glColor3f(0.3f, 0.8f, 0.25f);

        gl.glVertex3f(4.25f, -0.5f, 0.0f);
        gl.glVertex3f(3.9f, 0.0f, 0.0f);
        gl.glVertex3f(3.9f, 0.25f, 0.0f);
        gl.glVertex3f(4.5f, 0.25f, 0.0f);

        gl.glColor3f(0.5f, 0.8f, 0.4f);

        gl.glVertex3f(3.55f, -0.5f, 0.0f);
        gl.glVertex3f(3.9f, 0.0f, 0.0f);
        gl.glVertex3f(3.9f, 0.25f, 0.0f);
        gl.glVertex3f(3.3f, 0.25f, 0.0f);
        gl.glEnd();
    }
}
